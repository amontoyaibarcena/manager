import React, { Component } from 'react';
import './css/app.css';
import Header from './components/Header';
import Footer from './components/Footer';
import UserInfo from './components/UserInfo';
import Welcome from './components/Welcome';
import TabsNav from './components/Tabs';


class App extends Component {
  render() {
    return (
      <div className="App">
        <Header></Header>
        <div className="Content">
          <div className="UserWelcome">
            <UserInfo></UserInfo>
            <div className="Line">
              <img alt="" src={require('./images/line.png')} />
            </div>
            <Welcome></Welcome>
          </div>
          <div className="Detail">
            <TabsNav></TabsNav>
          </div>
        </div>
        <Footer></Footer>
      </div>
    );
  }
}

export default App;
