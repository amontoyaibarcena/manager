import React, { Component } from 'react';
import '../css/fancy-example.css';
import '../css/navcontent.css';
import {
  Accordion,
  AccordionItem,
  AccordionItemTitle,
  AccordionItemBody,
} from 'react-accessible-accordion';

class NavContent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      emails: [
        { key: 1, text: 'user@laborum.pe', isDefault: true },
        { key: 2, text: 'usuario@gmail.com', isDefault: false }
      ],
      phones: [],
      addingNewEmail: false,
      addingNewPhone: false,
      password: '123456',
    };
    this.deleteItem = this.deleteItem.bind(this);
    this.setDefault = this.setDefault.bind(this);
    this.addItem = this.addItem.bind(this);
    this.newEmail = this.newEmail.bind(this);
    this.newPhone = this.newPhone.bind(this);
    this.addNumber = this.addNumber.bind(this);
    this.deletePhone = this.deletePhone.bind(this);
    this.setDefaultPhone = this.setDefaultPhone.bind(this);
    this.changePassword = this.changePassword.bind(this);
  }

  newPhone(value) {
    this.setState({ addingNewPhone: value })
  }

  newEmail(value) {
    this.setState({ addingNewEmail: value })
  }

  addNumber(e) {
    if (this._inputNumber.value !== "") {
      var newItem = {
        isDefault: false,
        text: this._selectElement.value + this._inputNumber.value,
        key: Date.now()
      };
      this.setState((prevState) => {
        return {
          phones: prevState.phones.concat(newItem)
        };
      });
      this._inputNumber.value = "";
    }
    e.preventDefault();
  }

  setDefaultPhone(key) {
    const values = this.state.phones.map((phone) => {
      if (phone.key === key)
        phone.isDefault = true;
      else
        phone.isDefault = false;
      return phone;
    });
    this.setState({ phones: values })
  }

  deletePhone(key) {
    var filteredPhones = this.state.phones.filter(function (phone) {
      return (phone.key !== key);
    });

    this.setState({
      phones: filteredPhones
    });
  }

  addItem(e) {
    if (this._inputElement.value !== "") {
      var newItem = {
        isDefault: false,
        text: this._inputElement.value,
        key: Date.now()
      };
      this.setState((prevState) => {
        return {
          emails: prevState.emails.concat(newItem)
        };
      });
      this._inputElement.value = "";
    }
    e.preventDefault();
  }

  setDefault(key) {
    const values = this.state.emails.map((email) => {
      if (email.key === key)
        email.isDefault = true;
      else
        email.isDefault = false;
      return email;
    });
    this.setState({ emails: values })
  }

  deleteItem(key) {
    var filteredEmails = this.state.emails.filter(function (email) {
      return (email.key !== key);
    });

    this.setState({
      emails: filteredEmails
    });
  }

  changePassword(e) {
    if (this._inputOldPass.value === this.state.password && this._inputNewPass.value === this._inputNewPass2.value) {
      this.setState({ password: this._inputNewPass.value });
      alert("Nuevo password" + this._inputNewPass.value);
    }
    else
      alert("Error al cambiar password")
  }

  render() {
    return (
      <Accordion accordion={false}>
        <AccordionItem>
          <AccordionItemTitle className="Head">
            <div className="Head-div">
              <div className="Head-body">
                <span>Inicio de sesión y seguridad</span>
                <div>Añade o elimina direcciones de correo en tu cuenta.</div>
              </div>
              <div className="Icon">
                <img alt="" className="Icon" src={require('../images/arrow-icon.png')} />
              </div>
            </div>
          </AccordionItemTitle>
          <AccordionItemBody>
            <div className="Body">{this.state.emails.map((email) =>
              <ul className="List">
                <li className="ListItem" key={email.key}>
                  <div className="Item">
                    <div className="Email">
                      {email.text}
                    </div>
                    <div className="State">
                      {email.isDefault
                        ? <span>Correo principal</span>
                        : (<div>
                          <a href="#" onClick={() => this.setDefault(email.key)}>Seleccionar como principal</a>
                          <a href="#" onClick={() => this.deleteItem(email.key)}>Eliminar</a>
                        </div>)
                      }
                    </div>
                  </div>
                </li>
              </ul>)}
              <div>
                {this.state.addingNewEmail
                  ? <form onSubmit={this.addItem}>
                    <input placeholder="Ingresa tu correo electrónico" ref={(a) => this._inputElement = a} className="Input" type="text" name="name" />
                    <input className="Submit-button" type="submit" value="Enviar verificación" />
                    <a href="#" onClick={() => this.newEmail(false)}>Cancelar</a>
                  </form>
                  : <a href="#" onClick={() => this.newEmail(true)}>Añadir correo</a>
                }
              </div>
            </div>
          </AccordionItemBody>
        </AccordionItem>
        <AccordionItem>
          <AccordionItemTitle className="Head">
            <div className="Head-div">
              <div className="Head-body">
                <span>Números de celular</span>
                <div>Añade un número de celular para hacer más segura tu cuenta.</div>
              </div>
              <div className="Icon">
                <img alt="" className="Icon" src={require('../images/arrow-icon.png')} />
              </div>
            </div>
          </AccordionItemTitle>
          <AccordionItemBody>
            <div className="Empty">
              {(this.state.phones.length === 0 && !this.state.addingNewPhone) ?
                (<div>
                  <span>Aún no tienes ningún número de celular añadido a tu cuenta.</span>
                  <p>Tú número de teléfono nos ayuda a mantener la seguridad de tu cuenta. También
                    les ayuda a las personas que ya tienen tu número de teléfono a encontrarte y conectar contigo.
                  </p>
                  <button onClick={() => this.newPhone(true)} className="Submit-button">Añadir número</button>
                </div>)
                : (<div className="Body">{this.state.phones.map((phone) =>
                  <ul className="List">
                    <li className="ListItem" key={phone.key}>
                      <div className="Item">
                        <div className="Email">
                          {phone.text}
                        </div>
                        <div className="State">
                          {phone.isDefault
                            ? <span>Número principal</span>
                            : (<div>
                              <a href="#" onClick={() => this.setDefaultPhone(phone.key)}>Seleccionar como principal</a>
                              <a href="#" onClick={() => this.deletePhone(phone.key)}>Eliminar</a>
                            </div>)
                          }
                        </div>
                      </div>
                    </li>
                  </ul>)}
                  <div>
                    <form onSubmit={this.addNumber}>
                      <select className="Select" ref={(b) => this._selectElement = b}>
                        <option value="PE +51">Perú</option>
                        <option value="MX +11">México</option>
                        <option value="EU +01">USA</option>
                        <option value="AL +31">Austria</option>
                      </select>
                      <input placeholder="Ingresa tu número" maxlength="9" ref={(c) => this._inputNumber = c} className="Input" type="number" />
                      <input className="Submit-button" type="submit" value="Enviar código" />
                      <a href="#" onClick={() => this.newPhone(false)}>Cancelar</a>
                    </form>
                  </div>
                </div>)
              }
            </div>
          </AccordionItemBody>
        </AccordionItem>
        <AccordionItem>
          <AccordionItemTitle className="Head">
            <div className="Head-div">
              <div className="Head-body">
                <span>Cambiar contraseña</span>
                <div>Crea una constraseña única para proteger tu cuenta.</div>
              </div>
              <div className="Icon">
                <img alt="" className="Icon" src={require('../images/arrow-icon.png')} />
              </div>
            </div>
          </AccordionItemTitle>
          <AccordionItemBody>
            <div className="Password">
              <form onSubmit={this.changePassword}>
                <div>
                  <label>
                    Contraseña actual.
                  </label>
                </div>
                <div>
                  <input ref={(c) => this._inputOldPass = c} type="password" />
                </div>
                <div>
                  <label>
                    Contraseña nueva.
                  </label>
                </div>
                <div>
                  <input ref={(c) => this._inputNewPass = c} type="password" />
                </div>
                <div>
                  <label>
                    Vuelve a escribir tu contraseña nueva.
                  </label>
                </div>
                <div>
                  <input ref={(c) => this._inputNewPass2 = c} type="password" />
                </div>
                <input className="Submit-button" type="submit" value="Guardar" />
              </form>
            </div>
          </AccordionItemBody>
        </AccordionItem>
      </Accordion>
    );
  }
}

export default NavContent;