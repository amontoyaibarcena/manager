import React, { Component } from 'react';
import '../css/footer.css';

class Footer extends Component {
  render() {
    return (
      <div className='Footer'>
        <p>Krowdy Corporation © 2018</p>
      </div>
    );
  }
}

export default Footer;