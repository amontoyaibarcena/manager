import React, { Component } from 'react';
import '../css/react-web-tabs.css';
import { Tabs, Tab, TabPanel, TabList } from 'react-web-tabs';
import SideNavbar from '../components/SideNavbar';


class TabsNav extends Component {
  render() {
    return (
      <Tabs defaultTab="one">
        <TabList>
          <Tab tabFor="one">Cuenta</Tab>
          <Tab tabFor="two" disabled>Item</Tab>
        </TabList>
        <TabPanel tabId="one">
          <div className="SideNav">
            <SideNavbar></SideNavbar>
          </div>
        </TabPanel>
        <TabPanel tabId="two">

        </TabPanel>
      </Tabs>
    );
  }
}

export default TabsNav;