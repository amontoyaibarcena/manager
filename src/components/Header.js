import React, { Component } from 'react';
import '../css/header.css';

class Header extends Component {
  render() {
    return (
      <header className="Header">
        <div className="div-Header">
          <img alt="" className="Logo" src={require('../images/logo.png')} />
          Mario Canepa
        </div>
      </header>
    );
  }
}

export default Header;