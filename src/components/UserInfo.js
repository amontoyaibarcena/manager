import React, { Component } from 'react';
import '../css/userinfo.css';

class UserInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: 'Walter Ayala',
      email: 'walterayala@gmail.com',
      subscribed_since: '14 de Junio 2017'
    };
  }
  render() {
    return (
      <div className="UserInfo">
        <img alt="" className="Avatar" src={require('../images/perfil.png')} />
        <div className="Info">
          <span style={styles.name}>{this.state.name}</span><br />
          <span style={styles.email}>{this.state.email}</span><br />
          <br></br>
          <span style={styles.since}>Miembro desde {this.state.subscribed_since}</span>
        </div>
      </div>
    );
  }
}

var styles = {
  name: {
    fontSize: '20px',
  },
  email: {
    fontSize: '14px',
  },
  since: {
    fontSize: '12px',
  }

}

export default UserInfo;