import React, { Component } from 'react';
import '../css/welcome.css';

class Welcome extends Component {

  render() {
    return (
      <div className="Welcome">
        <div className="Description">
          <span style={styles.title}>Bienvenido</span>
          <p>Desde aquí y con tu cuenta de Krowdy podrás acceder rápidamente a tus herramientas y funciones para proteger tus datos y tu privacidad.</p>
        </div>
        <div>
          <img alt="" className="Picture" src={require('../images/imagen.png')} />
        </div>
      </div>
    );
  }
}

var styles = {
  title: {
    fontSize: '24px',
  }
}

export default Welcome;