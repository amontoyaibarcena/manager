import React, { Component } from 'react';
import '../css/sidenavbar.css';
import NavContent from '../components/NavContent';

const contents = [<NavContent></NavContent>, "", "", "", ""]

class SideNavbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      item: 0,
    }
  }

  handleClick(e, data) {
    this.setState({ item: e })
  }
  render() {
    return (
      <div className="Main">
        <div className="Navbar">
          <a style={styles.active} onClick={this.handleClick.bind(this, 0)}>Inicio de Sesión y seguridad</a>
          <a onClick={this.handleClick.bind(this, 1)}>Números de celular</a>
          <a onClick={this.handleClick.bind(this, 2)}>Cambiar contraseña</a>
          <a onClick={this.handleClick.bind(this, 3)}>Conexiones</a>
          <a onClick={this.handleClick.bind(this, 4)}>Gestión de cuenta</a>
        </div>
        <div className="NavContent">
          {contents[this.state.item]}
        </div>
      </div>

    );
  }
}

var styles = {
  active: {
    color: '#FF8B08',
  }
}

export default SideNavbar;